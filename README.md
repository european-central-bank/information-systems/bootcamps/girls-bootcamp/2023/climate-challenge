# Disclaimer
This project and its contents should not be reported as representing the views of the European Central Bank (ECB). The views expressed are those of the authors and do not necessarily represent those of the ECB.


# Introduction

Find here some code and solution proposals for the [ECB Girls Bootcamp 2023](https://www.ecb.europa.eu/ecb/educational/youth-initiatives/girls_it_bootcamp/html/index.en.html). 

Note: They are provided as-is. The instructions for setting up an environment are provided as is and the user is responsible for configuring it securely and making sure no malware is installed.

If you find a mistake or have improvement proposals, then please create an issue to this project (e.g. in [Gitlab](https://docs.gitlab.com/ee/user/project/issues/create_issues.html)) and/or provide an improvement as merge request/pull requests (e.g. in [Gitlab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)). See also our [contributor covenant code of conduct](CONTRIBUTING.md).

***See also at the bottom the section "How to continue" on more datasets to explore and how you can publish your work.***

# License

The code is licensed under the European Union Public License 1.2 ([EUPL-1.2](https://spdx.org/licenses/EUPL-1.2) or in [this repository](EUPL-1.2-EN.txt)).

The data is licensed by https://ourworldindata.org/co2-emissions

# Setup
Note: Please always check the installation sources to ensure no malware is included. This includes all software and extensions that you may install. Make sure that the packages/extensions have been created by a trustworthy party and make sure that they come indeed from this party.


## Visual Studio Code
[Visual Studio Code](https://code.visualstudio.com/) is an [Integrated Development Environment](https://en.wikipedia.org/wiki/Integrated_development_environment) (IDE) that can be used for development. You can use it with many programming languages by installing the programming language and some extensions. They are many other IDEs and you are free to choose another one for development. For example, for Python you have [PyCharm](https://en.wikipedia.org/wiki/PyCharm), [Spyder](https://en.wikipedia.org/wiki/Spyder_(software)) etc. It is often a matter of personal preference and what your organization/team has experience in. You are of course free to use just a simple text editor.

In fact, we used a very special Python environment in visual studio code called [Jupyter](https://en.wikipedia.org/wiki/Project_Jupyter), which is often used by data scientist to write reports that are filled with results from code directly to make an analysis understandable and reproducible.
You can also install [VSCodium](https://github.com/VSCodium/vscodium) which is a free open-source clone of Visual Studio Code. It works also with the extensions mentioned below.


Steps:
* Download Visual Studio Code: https://visualstudio.microsoft.com/downloads/
* Follow the instructions of the setup program

## Visual Studio Code Extensions

You can install extensions to VS Code / VS Codium directly in the application from the marketplace. Make sure that you only install trustworthy extensions by known vendors.

You can find a video and instructions on how to install extensions here (for VSCodium it is nearly the sme): https://code.visualstudio.com/learn/get-started/extensions

For this bootcamp we used the following extensions:
* [Python by Microsoft](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
* [Jupyter by Microsoft](https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter)
* [JavaScript and TypeScript Nightly by Microsoft](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-next)

## Python


Mambaforge by [Quantstack](https://quantstack.net/) is a Python distribution that allows you to install different Python versions and libraries. You can download it here: https://github.com/conda-forge/miniforge#mambaforge
It also allows installing other programming languages, such as the statistical programming language [R](https://www.r-project.org/).

You need to select your operating system (often it is Windows), e.g. for Windows the link is: https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-Windows-x86_64.exe

Follow the instructions of the install.

You probably remember that we used libraries written by others in our code. We need to install them. 


***Make sure that you only install trustworthy libraries from trustworthy sources!***

To install them, go to the Windows Start Menu and select "Python Mambaforge" and select "Miniforge Prompt (Mambaforge)". Enter there 
```
mamba create -y -n bootcamp2023 python=3.11 folium geopandas nbformat numpy plotly jupyter scikit-learn
```

This will create a Python environment with a specific Python version and specific libraries.

Note: When you open the Jupyter notebooks provided here in VSCode then you will be asked to select an environment. Select the environment "bootcamp2023".

You can install libraries also in the Jupyter notebook directly. Simply enter the following in a cell:
```
!pip install <LIBRARY_NAME>
```
You need to replace <LIBRARY_NAME> by the name of the library (without < >).

You can also install libraries using
```
!mamba install -y <LIBRARY_NAME>
```


# Data 
You can find the data in the [data](data) folder. It was originally downloaded from: https://ourworldindata.org/co2-emissions

# Python Code

The Python code can be found in form of Jupyter notebooks in the folder [python](python). Make sure the that you have the extension for Jupyter installed, so you can view them properly. 

Find here more on how to work with Jupyter notebooks in VS Code: https://code.visualstudio.com/docs/datascience/jupyter-notebooks

# How to continue
We recommend that you publish your analysis on existing code repositories on the Internet so that others can learn from you. Billions of people do this already and every even small piece of code is important!

You can share them on - for example - there are many more:
* [Codeberg](https://www.codeberg.org) - a European non-profit organizations that allows sharing of open-source for free
* [Github](https://github.com/explore) - a very popular open-source sharing platforn (US-based for profit-organization) that allows sharing of open-source for free
* ... many more (try to find one which you agree most with and that have data privacy that is adequate for you)

You can also explore what code others shared on those platforms related to climate change!

More climate data:
* [ECB publishes new climate-related statistical indicators to narrow climate data gap](https://www.ecb.europa.eu/press/pr/date/2023/html/ecb.pr230124~c83dbef220.en.html) - one blog post by ECB sharing climate data
* [Climatetrace.org](https://www.climatetrace.org/)

Why not publish your code for any of the additional climate data?

# Contact
If you have general questions about the ECB IT Bootcamp, then please contact: it_bootcamp@ecb.europa.eu
